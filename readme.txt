"Charming" is a match-3 variant with the following rules:

Highlight "Charms" to remove them from the board and score points.
Charms may be linked by color or shape.
Charms must be adjacent to be linked.
