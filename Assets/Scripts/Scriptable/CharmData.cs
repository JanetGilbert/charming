﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/CharmData", order = 1)]
public class CharmData : ScriptableObject
{
    public float fallSpeed;
    public float fadeSpeed;

    // Charm sprites
    public Sprite[] charmSprites;
}
