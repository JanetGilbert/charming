﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Description of gameplay changes on level up.
[System.Serializable]
public class LevelData 
{
    public CharmType type; // Highest charm type used in level.
    public CharmColor color; // Highest charm color used in level.
    public int LevelThreshold; // Number of charms needed to be matched to progress to next level.
}

// Mapping a color enum to an actual Color value.
[System.Serializable]
public class ColorMapData
{
    public CharmColor colorEnum;
    public Color colorVal; 
}