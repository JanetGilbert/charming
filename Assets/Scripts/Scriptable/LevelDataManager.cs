﻿using UnityEngine;


// Stores data about the game.
[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/LevelDataScriptableObject", order = 1)]
public class LevelDataManager : ScriptableObject
{
    public LevelData [] data;
    public ColorMapData[] colorMap;

    public int gridW;
    public int gridH;
    public int matchLen;

    //public float fadeSpeed;// = 2.0f; // How fast charms fade when removed.
  //  public float fallSpeed;//= 5.0f; // How fast charms fall.
    public float shakeTimeMax;// = 2.0f; // How long charms shake on Game Over.
    public float shakeFreq;// = 0.05f; // How frequently to shake the board in the game over state.
    public float fadeDelayMax;// = 0.8f; // Staggers fade on match so that the line appears to disappear in order.
    public float fadeStepMin;// = 0.2f; // Minimum pause between fading charms.


    // Generate a random charm color based on level (higher levels have more colors).
    public CharmColor RandomColor(int level)
    {
        return (CharmColor)(UnityEngine.Random.Range(0, (int)(data[level - 1].color) + 1));
    }

    // Generate a random charm type based on level (higher levels have more types).
    public CharmType RandomCharmType(int level)
    {
        return (CharmType)(UnityEngine.Random.Range(1, (int)(data[level - 1].type) + 1));
    }

    // How many charms must be matched to get to the next level.
    public int GetLevelThreshold(int level)
    {
        return (int)data[level - 1].LevelThreshold;
    }

    // Translate the charm color enum to a displayable Color.
    public Color TranslateCharmColor(CharmColor newCol)
    {
        return colorMap[(int)newCol].colorVal;
    }
}


