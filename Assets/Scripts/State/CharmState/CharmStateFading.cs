﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Charm is fading out before being destroyed.
public class CharmStateFading : IState<CharmStateEnum, Charm>
{
    public void Enter(Charm charm)
    {
    }

    public void Execute(Charm charm)
    {
        if (charm.FadeDelay > 0.0f)
        {

            charm.FadeDelay -= Time.deltaTime;

            if (charm.FadeDelay <= 0.0f)
            {
                // Set off particle explosion.
                charm.StartMatchParticles();     
            }
        }
        else
        {
            charm.AlphaLevel -= Time.deltaTime * charm.data.fadeSpeed;

            if (charm.AlphaLevel <= 0.0f)
            {
                charm.AlphaLevel = 0.0f;
            }

           
        }
    }

    public void Exit(Charm charm)
    {
    }

    public CharmStateEnum GetID()
    {
        return CharmStateEnum.Fading;
    }
}