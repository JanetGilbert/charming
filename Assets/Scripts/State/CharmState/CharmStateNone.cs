﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Default state.
public class CharmStateNone : IState<CharmStateEnum, Charm>
{
    public void Enter(Charm charm)
    {
    }

    public void Execute(Charm charm)
    {

    }

    public void Exit(Charm charm)
    {
    }

    public CharmStateEnum GetID()
    {
        return CharmStateEnum.None;
    }
}