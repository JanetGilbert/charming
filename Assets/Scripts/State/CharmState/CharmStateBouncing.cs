﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Charms bounce out of screen on Game Over.
public class CharmStateBouncing : IState<CharmStateEnum, Charm>
{
    public void Enter(Charm charm)
    {
    }

    public void Execute(Charm charm)
    {
        charm.transform.Translate(charm.BounceDir * Time.deltaTime);
    }

    public void Exit(Charm charm)
    {
    }

    public CharmStateEnum GetID()
    {
        return CharmStateEnum.Bouncing;
    }
}