﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Charm is falling in the grid.
public class CharmStateFalling : IState<CharmStateEnum, Charm>
{
    public void Enter(Charm charm)
    {
    }

    public void Execute(Charm charm)
    {
        charm.transform.Translate(Vector3.down * Time.deltaTime * charm.data.fallSpeed);

        if (charm.transform.position.y <= charm.TargetY)
        {
            charm.transform.position = new Vector3(charm.transform.position.x, charm.TargetY, 0.0f);
            charm.stateMachine.ChangeState(Charm.charmStateNone, charm);
            charm.PlayFallEffect();
        }
    }

    public void Exit(Charm charm)
    {
    }

    public CharmStateEnum GetID()
    {
        return CharmStateEnum.Falling;
    }
}