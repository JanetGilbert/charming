﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Charm is destroyed.
public class CharmStateDestroyed : IState<CharmStateEnum, Charm>
{
    public void Enter(Charm charm)
    {
    }

    public void Execute(Charm charm)
    {

    }

    public void Exit(Charm charm)
    {
    }

    public CharmStateEnum GetID()
    {
        return CharmStateEnum.Destroyed;
    }
}