﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Default state.
public class BoardStateNone : IState<BoardStateEnum, Board>
{
    public void Enter(Board board)
    {
        Debug.Log("entering BoardStateNone");
    }

    public void Execute(Board board)
    {

    }

    public void Exit(Board board)
    {
        Debug.Log("exiting BoardStateNone");
    }

    public BoardStateEnum GetID()
    {
        return BoardStateEnum.None;
    }
}