﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Charms falling after match fades out.
public class BoardStateFalling : IState<BoardStateEnum, Board>
{
    public void Enter(Board board)
    {
        Debug.Log("entering BoardStateFalling");
    }

    public void Execute(Board board)
    {
        bool finishedFalling = true;

        for (int x = 0; x < board.GridW; x++)
        {
            for (int y = 0; y < board.GridH; y++)
            {
                board.charmGrid[x, y].Update();

                if (board.charmGrid[x, y].stateMachine.GetID() == CharmStateEnum.Falling)
                {
                    finishedFalling = false;
                }
            }
        }

        if (finishedFalling)
        {
            if (board.FindMatchStart() != null)
            {
                board.stateMachine.ChangeState(Board.boardStatePlaying, board); // todo make more elegant
            }
            else
            {
                board.stateMachine.ChangeState(Board.boardStateShaking, board);

            }

        }
    }

    public void Exit(Board board)
    {
        Debug.Log("exiting BoardStateFalling");
    }

    public BoardStateEnum GetID()
    {
        return BoardStateEnum.Falling;
    }
}
