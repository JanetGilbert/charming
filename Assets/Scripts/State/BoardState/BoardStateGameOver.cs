﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


// Game over
public class BoardStateGameOver : IState<BoardStateEnum, Board>
{

    public void Enter(Board board)
    {
        Debug.Log("entering BoardStateGameOver");
        board.sound.effectsPlayer.PlayOneShot(board.sound.effectGameOver);
    }

    public void Execute(Board board)
    {
        bool finished = true;
        for (int x = 0; x < board.GridW; x++)
        {
            for (int y = 0; y < board.GridH; y++)
            {
                board.charmGrid[x, y].Update();

                if (!board.charmGrid[x, y].IsOffscreen())
                {
                    finished = false;
                }
            }
        }

        if (finished)
        {
            board.stateMachine.ChangeState(Board.boardStateNone, board);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name); // Reload game
        }
    }

    public void Exit(Board board)
    {
        Debug.Log("exiting BoardStateGameOver");
    }

    public BoardStateEnum GetID()
    {
        return BoardStateEnum.GameOver;
    }
}
