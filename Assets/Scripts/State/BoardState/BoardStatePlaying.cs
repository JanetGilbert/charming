﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Game accepting player input.
public class BoardStatePlaying : IState<BoardStateEnum, Board>
{
    public void Enter(Board board)
    {
        Debug.Log("entering BoardStatePlaying");
    }

    public void Execute(Board board)
    {
        board.UpdateHintTimer();

        if (Input.GetMouseButton(0))
        {
            board.SelectCurrentCharm();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            board.RemoveLine(false);
        }

        // Cheat
        /*if (Input.GetKeyDown(KeyCode.E))
        {
            board.stateMachine.ChangeState(Board.boardStateShaking, board);
        }*/

        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Q))
        {
            Application.Quit();
        }
    }

    public void Exit(Board board)
    {
        Debug.Log("exiting BoardStatePlaying");
    }

    public BoardStateEnum GetID()
    {
        return BoardStateEnum.Playing;
    }
}

