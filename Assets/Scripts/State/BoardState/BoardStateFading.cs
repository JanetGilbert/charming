﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Match fading out before charms disappear.
public class BoardStateFading: IState<BoardStateEnum, Board>
{
    public void Enter(Board board)
    {
        Debug.Log("entering BoardStateFading");
    }

    public void Execute(Board board)
    {
        bool finishedFading = true;

        for (int x = 0; x < board.GridW; x++)
        {
            for (int y = 0; y < board.GridH; y++)
            {
                board.charmGrid[x, y].Update();

                if (!board.charmGrid[x, y].IsFadeFinished())
                {
                    finishedFading = false;
                }
            }
        }

        if (finishedFading)
        {
            board.SetCharmsFalling();
            board.AddNewCharms();

            board.stateMachine.ChangeState(Board.boardStateFalling, board);
        }
    }

    public void Exit(Board board)
    {
        Debug.Log("exiting BoardStateFading");
    }

    public BoardStateEnum GetID()
    {
        return BoardStateEnum.Fading;
    }
}
