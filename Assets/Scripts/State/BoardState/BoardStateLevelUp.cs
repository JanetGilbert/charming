﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Between levels.
public class BoardStateLevelUp : IState<BoardStateEnum, Board>
{
    public void Enter(Board board)
    {
        Debug.Log("entering BoardStateLevelUp");
        board.sound.effectsPlayer.PlayOneShot(board.sound.effectLevelUp);
    }

    public void Execute(Board board)
    {
        bool levelFinished = true;

        for (int x = 0; x < board.GridW; x++)
        {
            for (int y = 0; y < board.GridH; y++)
            {
                board.charmGrid[x, y].Update();

                if (!board.charmGrid[x, y].IsFadeFinished())
                {
                    levelFinished = false;
                }
            }
        }

        if (levelFinished)
        {
            board.StartLevel(board.stats.Level + 1);
        }
    }

    public void Exit(Board board)
    {
        Debug.Log("exiting BoardStateLevelUp");
    }

    public BoardStateEnum GetID()
    {
        return BoardStateEnum.LevelUp;
    }
}
