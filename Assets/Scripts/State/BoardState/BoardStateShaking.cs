﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Board shakes before entering Game Over state.
public class BoardStateShaking : IState<BoardStateEnum, Board>
{
    public void Enter(Board board)
    {
        Debug.Log("entering BoardStateShaking");
        board.ShakeTime = board.level.shakeTimeMax;
    }

    public void Execute(Board board)
    {
        board.ShakeTime -= Time.deltaTime;
        board.ShakePeriod -= Time.deltaTime;

        if (board.ShakePeriod <= 0.0f)
        {
            for (int x = 0; x < board.GridW; x++)
            {
                for (int y = 0; y < board.GridH; y++)
                {
                    board.charmGrid[x, y].Shake();
                }
            }

            board.ShakePeriod = board.level.shakeFreq;
        }

        if (board.ShakeTime <= 0.0f)
        {
            board.stateMachine.ChangeState(Board.boardStateGameOver, board);

            for (int x = 0; x < board.GridW; x++)
            {
                for (int y = 0; y < board.GridH; y++)
                {
                    board.charmGrid[x, y].Bounce(); // Charms bounce out of grid - Game Over.
                }
            }

            board.ShakeTime = 0.2f; // Pause time before Game over to allow bounce animation to play.
        }
    }

    public void Exit(Board board)
    {
        Debug.Log("exiting BoardStateShaking");
    }

    public BoardStateEnum GetID()
    {
        return BoardStateEnum.Shaking;
    }
}
