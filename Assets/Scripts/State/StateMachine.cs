﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IState<E, T>
{
    void Enter(T t);
    void Execute(T t);
    void Exit(T t);
    E GetID();
}

public class StateMachine<E, T> where E : System.Enum
{
    IState<E, T> currentState;

    public void ChangeState(IState<E, T> newState, T user)
    {
        if (currentState != null)
            currentState.Exit(user);

        currentState = newState;
        currentState.Enter(user);
    }

    public void Update(T user)
    {
        if (currentState != null) currentState.Execute(user);
    }

    public E GetID()
    {
        return currentState.GetID();
    }
}


