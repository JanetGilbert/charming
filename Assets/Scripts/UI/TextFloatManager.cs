﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Keep a pool of floating text effects to display when player scores points.
// They are recycled to save on speed and garbage collection.
public class TextFloatManager : MonoBehaviour
{
    private const int maxTextFloats = 10; // Maximum number of text floats on screen at once.
    private TextFloat[] textFloats; // The pool of floating text.

    [Header("Adjustable settings")]
    public float floatLife = 2.0f; // Lifetime of floating text.
    public float floatSpeed = 3.0f; // Speed that floating text travels up screen.

    [Header("Prefabs - Must be set in Inspector")]
    public TextFloat textFloatPrefab;


    void Start()
    {
        textFloats = new TextFloat[maxTextFloats];
    }


    void Update()
    {

    }

    // Add a new text float to the pool (recycling an old one if possible.)
    public void AddTextFloat(Color color, string newText, Vector2 pos, int fontSize)
    {
        TextFloat newFloat = null;

        // Find an unused floating text.
        foreach (TextFloat tf in textFloats)
        {
            if (tf != null && tf.IsFinished())
            {
                newFloat = tf;

                break;
            }
        }

        // Add a new floating text;
        if (newFloat == null)
        {
            // Find an unused floating text.
            for (int itf = 0; itf < textFloats.Length; itf++)
            {
                if (textFloats[itf] == null)
                {
                    textFloats[itf] = Instantiate(textFloatPrefab, transform);
                    textFloats[itf].transform.position = pos;
                    newFloat = textFloats[itf];
                    break;
                }
            }
        }

        // Re-use the oldest alive floating text.
        if (newFloat == null)
        {
            float leastLife = floatLife * 2.0f;

            foreach (TextFloat tf in textFloats)
            {
                if (tf != null && tf.Lifetime < leastLife)
                {
                    newFloat = tf;
                    leastLife = tf.Lifetime;

                    break;
                }
            }
        }

        if (newFloat)
        {
            newFloat.Setup(color, newText, pos, floatLife, floatSpeed, fontSize);
        }
    }
}

