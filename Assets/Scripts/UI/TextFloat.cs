﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// A string that floats upscreen and fades out.
// Used for displaying the score for a match.
public class TextFloat : MonoBehaviour
{
    private float lifetime;
    private float maxLifetime;
    private float speed = 1.0f;
    private Renderer textRenderer;
    private TextMeshPro textMeshPro;

    public float Lifetime
    {
        get
        {
            return lifetime;
        }
    } 

    void Awake()
    {
        textRenderer = GetComponent<Renderer>();
        textMeshPro = GetComponent<TextMeshPro>();
    }


    void Update()
    {
        if (lifetime > 0.0f)
        {
            transform.position = new Vector2(transform.position.x, transform.position.y + (speed * Time.deltaTime));

            lifetime -= Time.deltaTime;

            // Fade
            float alpha = lifetime / maxLifetime;
            textMeshPro.color = new Color(textMeshPro.color.r, textMeshPro.color.g, textMeshPro.color.b, alpha);


            if (lifetime <= 0.0f)
            {
                textRenderer.enabled = false;
            }
        }

        
    }

    // Initialise Text float
    public void Setup(Color color, string display, Vector2 pos, float newLife, float newSpeed, int fontSize)
    {
        lifetime = newLife;
        maxLifetime = newLife;
        transform.position = pos;
        textRenderer.enabled = true;
        textMeshPro.faceColor = color;
        textMeshPro.text = display;
        textMeshPro.fontSize = fontSize;
        speed = newSpeed;
    }

    // Is the text float available for re-use?
    public bool IsFinished()
    {
        return !textRenderer.enabled;
    }
    
}
