﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [Header("Audio Sources - Must be set in Inspector")]
    public AudioSource musicPlayer;
    public AudioSource effectsPlayer;

    // Effects
    [Header("Sound effects - Must be set in Inspector")]
    public AudioClip effectLevelUp;
    public AudioClip effectMatch;
    public AudioClip effectRemoveCharm;
    public AudioClip effectSelectCharm;
    public AudioClip effectCharmFall;
    public AudioClip effectGameOver;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

}
