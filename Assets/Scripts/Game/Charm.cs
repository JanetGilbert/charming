﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Represents a single charm in the grid.
public class Charm : MonoBehaviour
{
    // Position in grid
    private int colX;
    private int rowY;

    public int ColX { get { return colX; } }
    public int RowY { get { return rowY; } }

    // SO
    public CharmData data;

    // Charm type

    public CharmType CharmType { get; set; }

    // Charm state
    public bool IsSelected { get; set; }
    public Color Tint { get; set; }
    // public CharmState State { get; set; }

 
    // Bouncing
    private Vector2 bounceDir; // Direction that charms bounce off the board on Game Over.
    private const float bounceSpeed = 6.0f; // Speed that charms bounce off the board on Game Over.

    [Header("Particle prefab - Must be set in Inspector")]
    public ParticleSystem particlesPrefab;

    // Instantiated match effect particle systen.
    private ParticleSystem particles;

    // State
    public StateMachine<CharmStateEnum, Charm> stateMachine;
    public static CharmStateBouncing charmStateBouncing = new CharmStateBouncing();
    public static CharmStateDestroyed charmStateDestroyed = new CharmStateDestroyed();
    public static CharmStateFading charmStateFading = new CharmStateFading();
    public static CharmStateFalling charmStateFalling = new CharmStateFalling();
    public static CharmStateNone charmStateNone = new CharmStateNone();

    // Cached shortcuts
    private Board board;
    private SpriteRenderer charmRenderer;

    // Game logic

    private float targetY; // Target position for when new Charms fall in.
    public float FadeDelay { get; set; } // Delay until cell fades out, to create a nice effect after matches.


    // Getters and Setters
    private CharmColor charmColor;
    public CharmColor CharmColor
    {

        get { return charmColor; }

        set
        {
            charmColor = value;

            bool doTint = true;

            if (stateMachine.GetID() == CharmStateEnum.Fading)
            {
                doTint = false;
            }

            if (doTint)
            {
                Tint = board.level.TranslateCharmColor(CharmColor);
                charmRenderer.color = Tint;
            }
        }
    }

    // For fading out Charm when matched.
    private float alphaLevel;
    public float AlphaLevel {

        get
        {
            return alphaLevel;
        }
        set
        {
            alphaLevel = value;
            charmRenderer.color = new Color(Tint.r, Tint.g, Tint.b, alphaLevel);
        }

    } 

    public float TargetY
    {
        get
        {
            return targetY;
        }
    }

    public Vector2 BounceDir
    {
        get
        {
            return bounceDir;
        }
    }



    public void Awake()
    {
        // States

        charmRenderer = GetComponent<SpriteRenderer>();
        stateMachine = new StateMachine<CharmStateEnum, Charm>();
        stateMachine.ChangeState(charmStateNone, this);         // Initialize state machine

    }

    public void Start()
    {

    }

    // Initialise Charm.
    public void Init(int x, int y, CharmColor ccol, CharmType ctype, SpecialCharmType cspecial)
    {


        board = transform.parent.GetComponent<Board>();
        charmRenderer.sprite = data.charmSprites[(int)ctype - 1];

        //Vector3 newPos = new Vector3(board.GridStartX + (board.GridSpacingX * x), board.GridStartY + (board.GridSpacingY * y));

        // General setup
        stateMachine.ChangeState(charmStateNone, this);
        SetPos(x, y);
        CharmColor = ccol;
        CharmType = ctype;

        name = "Charm " + x + " " + y; // Useful to know which charm is which for debugging.

        // Set up particles.
        particles = GetComponentInChildren<ParticleSystem>();

        if (particles == null)
        {
            particles = Instantiate(particlesPrefab, transform);
        }

        particles.transform.position = new Vector3(0.0f, 0.0f, 0.0f);

        // Set Transparency
        AlphaLevel = 1.0f;
        charmRenderer.color = new Color(Tint.r, Tint.g, Tint.b, AlphaLevel);
    }

    public void Update()
    {
        stateMachine.Update(this);
    }

    // Set grid position and update transform.
    public void SetPos(int posx, int posy)
    {
        colX = posx;
        rowY = posy;

        transform.position = new Vector3(board.GridStartX + (board.GridSpacingX * posx), board.GridStartY + (board.GridSpacingY * posy), 0.0f);
    }

    // Get the world position of the center of the charm.
    public Vector3 GetCenterPos()
    {
        return transform.position + charmRenderer.sprite.bounds.center;
    }

    // Start charm fading out after match.
    public void StartFade(float delay)
    {
        stateMachine.ChangeState(charmStateFading, this);
        FadeDelay = delay;
        AlphaLevel = 1.0f;
    }

    // Is the charm faded out after a match yet?
    public bool IsFadeFinished()
    {
        if (stateMachine.GetID() == CharmStateEnum.Fading)
        {
            if (!particles.isEmitting && AlphaLevel <= 0.01f && FadeDelay <= 0.0f)
            {
                return true;
            }
            return false;

        }

        return true;
    }

    // Set charm falling down the screen at start of game.
    public void SetFalling(float fallY)
    {
        stateMachine.ChangeState(charmStateFalling, this);
        targetY = transform.position.y;

        transform.position = new Vector3(transform.position.x, transform.position.y + fallY);
    }

    // Recalculate world position based on grid position.
    public void RefreshDisplayPos()
    {
        transform.position = new Vector3(board.GridStartX + (board.GridSpacingX * colX), board.GridStartY + (board.GridSpacingY * rowY));
    }

    // Use particle effects so that charm appears to explode on matching.
    public void StartMatchParticles()
    {
        ParticleSystem.MainModule psMain = particles.main;
        psMain.startColor = Tint;

        // Particle
        particles.transform.position = GetCenterPos();

        // Set particle sprite to have the same image as the Charm.
        var tsa = particles.textureSheetAnimation;
        tsa.rowIndex = ((int)CharmType) - 1; // Sprite sheet goes from bottom up.

        // Make the particle system semi-transparent.
        Color psTint = psMain.startColor.color;
        psTint.a = Random.Range(0.2f, 0.7f);
        psMain.startColor = psTint;

        // Play particle system 
        particles.Play();

        // Play sound effect
        board.sound.effectsPlayer.PlayOneShot(board.sound.effectRemoveCharm);
    }

    // Shake the Charm a little to prepare player for Game Over.
    public void Shake()
    {
        const float shakeMin = -0.1f;
        const float shakeMax = 0.1f;

        float newX = board.GridStartX + (board.GridSpacingX * colX) + UnityEngine.Random.Range(shakeMin, shakeMax);
        float newY = board.GridStartY + (board.GridSpacingY * rowY) + UnityEngine.Random.Range(shakeMin, shakeMax);

        transform.position = new Vector3(newX, newY, 0.0f);
    }

    // Create a single particle copy of the Charm to fall offscreen at Game Over.
    public void Bounce()
    {
        bounceDir = Random.insideUnitCircle.normalized * bounceSpeed;
        stateMachine.ChangeState(charmStateBouncing, this);
    }

    // Generate a random type of charm by level.
    public void Generate(int level)
    {
        SpecialCharmType specialType = SpecialCharmType.None;

        Init(colX, rowY, board.level.RandomColor(level), board.level.RandomCharmType(level), specialType);

        particles.Clear();
    }

    // Is the charm offscreen? Used for detecting when end of level bounce effect is finished.
    public bool IsOffscreen()
    {
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);

        return (screenPosition.x < 0 || screenPosition.x > Screen.width ||
            screenPosition.y < 0 || screenPosition.y > Screen.height);
    }

    // Play sound effect when charm stops falling.
    public void PlayFallEffect()
    {
        board.sound.effectsPlayer.PlayOneShot(board.sound.effectCharmFall);
    }
}
