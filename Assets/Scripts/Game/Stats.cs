﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


// Contains and displays level progress data.
public class Stats : MonoBehaviour
{
    private int score = 0;
    private int progress = 0;  // How many charms have been matched this level.
    private int maxProgress = 0; // How many charms need to be matched to get to the next level.
    private int level = 0;

    [Header("Linked objects - Must be set in Inspector")]
    public TextMeshProUGUI textScore;
    public TextMeshProUGUI textProgress;
    public TextMeshProUGUI textLevel;

    // Getters and setters.
    public int Score {
        get
        {
            return score;
        }

        set
        {
            score = value;

            if (textScore)
            {
                textScore.text = "Score: " + value.ToString();
            }
        }
    }

    public int Progress
    {
        get
        {
            return progress;
        }

        set
        {
            progress = value;

            if (textProgress)
            {
                float progressPercentage = Mathf.Floor(((float)progress / (float)maxProgress) * 100.0f);

                textProgress.text = "Progress: " + progressPercentage.ToString() + "%";
            }
        }
    }

    public int Level
    {
        get
        {
            return level;
        }

        set
        {
            level = value;

            if (textLevel)
            {
                textLevel.text = "Level: " + level.ToString();
            }
        }
    }


    void Start()
    {
        Score = 0;

    }

    public void Restart()
    {
        Score = 0;
        Progress = 0;
    }


    void Update()
    {
        
    }

    // Call at the start of every level.
    public void StartLevel(int newLevel, int levelThreshold)
    {
        maxProgress = levelThreshold;
        Progress = 0;
        Level = newLevel;
    }


}
