﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class keeps track of whether a particular cell grid is selected or not, and what should display there.

public class Selection : MonoBehaviour
{
    // Grid position
    private int colX;
    private int rowY;

    public int ColX { get { return colX; } }
    public int RowY { get { return rowY; } }



    [Header("Sprites - Must be set in Inspector")]
    public Sprite deselectedSprite;
    public Sprite selectedSprite;

    // Shortcuts
    private Board board;
    private SpriteRenderer cellRenderer;

    // Getters/Setters

    private bool selected; // Is this grid square selected?
    public bool Selected
    {
        get
        {
            return selected;
        }

        set
        {
            selected = value;

            if (selected)
            {
                cellRenderer.sprite = selectedSprite;
            }
            else
            {
                cellRenderer.sprite = deselectedSprite;
            }
        }
    }


    public void Init(int posx, int posy)
	{
        board = transform.parent.GetComponent<Board>();

        Vector3 start = new Vector3(board.GridStartX + (board.GridSpacingX * posx), board.GridStartY + (board.GridSpacingY * posy), 0.0f);

        transform.position = start;

        cellRenderer = GetComponent<SpriteRenderer>();

		colX = posx;
		rowY = posy;
	}

	

}

