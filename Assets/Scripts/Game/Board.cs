﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Game Logic enums
public enum CharmType { None, Heart, Star, Horseshoe, Shamrock, Moon, Treasure, Rainbow, Balloon, Special };
public enum SpecialCharmType { None, Wildcard, Bomb, Firework, PaintBomb, Blocker, BlockerMaker, PaintWorm }; // TODO - add special Charms.
public enum CharmColor { Pink, Blue, Cyan, Green, Purple, Red, Yellow, Orange, White };
public enum CharmStateEnum { None, Falling, Fading, Destroyed, Bouncing };
public enum BoardStateEnum { Playing, Falling, Fading, Shaking, GameOver, LevelUp, None };

// Main gameplay class: stores and manages the grid of charms.
public class Board : MonoBehaviour
{
    // Settable
    [Header("Prefabs - Must be set in Inspector")]
    public GameObject charmPrefab;
    public GameObject selectionPrefab;

    [Header("Linked objects - Must be set in Inspector")]
    public TextFloatManager textFloatManager;
    public Stats stats;
    public ParticleSystem hintSparkle;
    public LevelDataManager level;
    public SoundManager sound;
    public RectTransform playArea;

    // Calculated grid positions.
    private float gridSpacingX;
    private float gridSpacingY;
    private float gridStartX;
    private float gridStartY;

    public float GridSpacingX { get { return gridSpacingX; } }
    public float GridSpacingY { get { return gridSpacingY; } }
    public float GridStartX { get { return gridStartX; } }
    public float GridStartY { get { return gridStartY; } }

    // Gameplay shortcuts
    public int GridW { get { return level.gridW; } }
    public int GridH { get { return level.gridH; } }
    public int MatchLen { get { return level.matchLen; } }



    // Game logic
    [HideInInspector]
    public Charm[,] charmGrid; // The grid of instantiated charms. 0, 0  is bottom left.
    private Selection[,] selectionGrid; // The current selected area (as grid).
    private List<Selection> selectionLine; // The current selected area (as line).
    private Charm curCharm; // The currently selected charm.
    private bool[,] tempPieces; // Temporary grid used in matching algorithm.
    private float hintTimer; // How long until the next hint is played? (Hints are subtle sparkles over the start of possible matches).

    // Getters/Setters
    public float ShakeTime { get; set; } // How long has the board been shaking in the game over state?
    public float ShakePeriod { get; set; } // How frequently to shake the board in the game over state.

    // State Machine
    public StateMachine<BoardStateEnum, Board> stateMachine = new StateMachine<BoardStateEnum, Board>();
    public static BoardStateFading boardStateFading = new BoardStateFading();
    public static BoardStateFalling boardStateFalling = new BoardStateFalling();
    public static BoardStateGameOver boardStateGameOver = new BoardStateGameOver();
    public static BoardStateLevelUp boardStateLevelUp = new BoardStateLevelUp();
    public static BoardStateNone boardStateNone = new BoardStateNone();
    public static BoardStatePlaying boardStatePlaying = new BoardStatePlaying();
    public static BoardStateShaking boardStateShaking = new BoardStateShaking();

    public void Start()
    {
        // Initialize member variables.
        charmGrid = new Charm[GridW, GridH];
        selectionGrid = new Selection[GridW, GridH];
        selectionLine = new List<Selection>();
        tempPieces = new bool[GridW, GridH];
        curCharm = null;

        // States
        stateMachine.ChangeState(boardStatePlaying, this);

        // Calculate grid spacing and position (centered on screen)
       // Vector3 spriteSize = charmSprites[0].bounds.size;
        Vector2 screenCenter = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2.0f, Screen.height / 2.0f));
        Vector3 [] playCorners = new Vector3[4];
        playArea.GetWorldCorners(playCorners); // 0 – Bottom Left 1 – Top Left 2 – Top Right 3 – Bottom Right

        foreach (Vector3 corner in playCorners) { Debug.Log(corner.x + " " + corner.y); };

        gridSpacingX = Mathf.Abs(playCorners[3].x - playCorners[1].x)/GridW;
        gridSpacingY = Mathf.Abs(playCorners[3].y - playCorners[1].y)/GridH;
        gridStartX = playCorners[0].x + gridSpacingX;// screenCenter.x - ((GridW-1) * gridSpacingX)/2.0f;
        gridStartY = playCorners[0].y + gridSpacingY;// screenCenter.y - ((GridH-1) * gridSpacingY)/2.0f;

        Debug.Log("gridspacing " + gridSpacingX + " " + gridSpacingY);

        // Scale board to screen resolution.
       // Camera.main.orthographicSize = gridSpacingX * GridW;

        // Set up initial board state.
        GameObject newCharm;
        GameObject newSelection;
        for (int x = 0; x < GridW; x++)
        {
            for (int y = 0; y < GridH; y++)
            {
                newCharm = Instantiate(charmPrefab);
                newCharm.transform.parent = transform;
                charmGrid[x, y] = newCharm.GetComponent<Charm>();
                charmGrid[x, y].Init(x, y, CharmColor.Pink, CharmType.Heart, SpecialCharmType.None);

                newSelection = Instantiate(selectionPrefab);
                newSelection.transform.parent = transform;
                selectionGrid[x, y] = newSelection.GetComponent<Selection>();
                selectionGrid[x, y].Init(x, y);
            }
        }

        ResetHintTimer();
        StartLevel(1);
    }

    // Restart game when player clicks on restart button.
    public void Restart()
    {
        ResetHintTimer();
        StartLevel(1);
        stats.Restart();
    }


    // Show hint when player clicks on hint button.
    public void ForceHint()
    {
        hintTimer = 0.0f;
    }

    // Main game logic update.
    void Update()
    {
        stateMachine.Update(this);
    }

    // Detect which charm the mouse is over.
    public void SelectCurrentCharm()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);

        if (hit && hit.collider != null)
        {
            if (hit.collider.tag == "Charm")
            {
                Charm charm = hit.collider.gameObject.GetComponent<Charm>();

                if (charm != null)
                {
                    if (IsPrevSelected(charm.ColX, charm.RowY)) // Player is deselecting the line by going back on it.
                    {
                        Selection prev = selectionLine[selectionLine.Count - 1];
                        Select(prev.ColX, prev.RowY, false);
                        sound.effectsPlayer.PlayOneShot(sound.effectSelectCharm);
                        curCharm = charm;
                    }
                    else // Select the charm under the mouse pointer.
                    {
                        if (DoesCharmConnect(curCharm, charm))
                        {
                            Select(charm.ColX, charm.RowY, true);
                            sound.effectsPlayer.PlayOneShot(sound.effectSelectCharm);
                            curCharm = charm;
                        }
                    }
                }
            }
        }
    }

    // Search the whole Charm grid for a match (favoring lower rows)
    // Used for hints, and to determine whether there are no possible matches (game over)
    public Charm FindMatchStart()
    {
        int offsetX = UnityEngine.Random.Range(0, GridW - 1); // Start from a random column so that the hint looks random.

        for (int tempX = -offsetX; tempX < GridW - offsetX; tempX++)
        {
            int x = tempX;

            if (tempX < 0)
            {
                x = GridW + tempX;
            }

            for (int y = 0; y < GridH; y++)
            {
                ClearTempPieces();

                if (CheckForMatch(x, y, charmGrid[x, y].CharmType, charmGrid[x, y].CharmColor) >= MatchLen)
                {
                    return charmGrid[x, y];
                }
            }
        }

        return null;
    }


    // Remove a line of matched Charms from the board.
    // If endOfLevel is true, the whole board is removed (end of level effect)
    public bool RemoveLine(bool endOfLevel)
    {
        int numSelected = selectionLine.Count;

        if (numSelected < MatchLen) // Match not long enough.
        {
            DeselectAll();
            return false;
        }

        sound.effectsPlayer.PlayOneShot(sound.effectMatch);
       
        Selection sel;
        Charm gridSel;
        Color scoreColor = Color.clear;
        Vector3 scorePos = new Vector3();

        // Delay to give a "wave" effect to the match.
        float delayStep = (endOfLevel) ? (level.fadeStepMin / 5.0f) : Mathf.Max(level.fadeDelayMax / selectionLine.Count, level.fadeStepMin);
        float delay = delayStep;

        // Iterate backwards over the selection.
        for (int selCur = selectionLine.Count - 1; selCur >= 0; selCur--)
        {
            sel = selectionLine[selCur];
            gridSel = charmGrid[sel.ColX, sel.RowY];
            gridSel.StartFade(delay);
            delay += delayStep;
            scorePos += gridSel.GetCenterPos();

            if (selCur == selectionLine.Count - 1) // First
            {
                scoreColor = gridSel.Tint;
            }
            else
            {
                scoreColor = (scoreColor + gridSel.Tint) / 2.0f; // Average color of selection to color floating score.
            }
        }

        DeselectAll();

        if (!endOfLevel)
        {
            stateMachine.ChangeState(boardStateFading, this);
            AddLevelProgress(scorePos, scoreColor, numSelected);
            ResetHintTimer();
        }

        return true;
    }

    // Set charms in each column falling to fill up the gaps.
    public void SetCharmsFalling()
    {
        int fall = 0;

        for (int x = 0; x < GridW; x++)
        {
            fall = 0;
            for (int y = 0; y < GridH; y++)
            {
                if (charmGrid[x, y].stateMachine.GetID() == CharmStateEnum.Fading || charmGrid[x, y].stateMachine.GetID() == CharmStateEnum.Destroyed)
                {
                    fall++;
                    charmGrid[x, y].stateMachine.ChangeState(Charm.charmStateDestroyed, charmGrid[x, y]);
                }
                else if (fall > 0)
                {
                    SwapCharms(ref charmGrid[x, y - fall], ref charmGrid[x, y]);
                    charmGrid[x, y - fall].SetFalling(fall * gridSpacingY);
                }
            }
        }
    }

    // Add new charms to the gaps caused by matches.
    public void AddNewCharms()
    {
        int x, y;
        int fall = 0;

        for (x = 0; x < GridW; x++)
        {
            fall = 0;

            for (y = GridH - 1; y >= 0; y--)
            {
                if (charmGrid[x, y].stateMachine.GetID() == CharmStateEnum.Destroyed)
                {
                    fall++;
                }
            }

            for (y = GridH - 1; y >= 0; y--)
            {
                if (charmGrid[x, y].stateMachine.GetID() == CharmStateEnum.Destroyed)
                {
                    charmGrid[x, y].Generate(stats.Level);
                    charmGrid[x, y].SetFalling((fall * gridSpacingY) - gridStartY);
                }
            }
        }
    }

    // After a certain amount of time, give player a hint.
    public void UpdateHintTimer()
    {
        hintTimer -= Time.deltaTime;

        if (hintTimer <= 0.0f)
        {
            ResetHintTimer();

            Charm matchStart = FindMatchStart();

            if (matchStart != null)
            {
                hintSparkle.transform.position = matchStart.GetCenterPos();
                hintSparkle.Play();
            }
        }
    }

    public void StartLevel(int newLevel)
    {
        stats.StartLevel(newLevel, level.GetLevelThreshold(newLevel));

        DeselectAll();
        ResetHintTimer();

        for (int x = 0; x < GridW; x++)
        {
            for (int y = 0; y < GridH; y++)
            {
                charmGrid[x, y].Generate(newLevel);

            }
        }

        // Set all charms falling.
        float fallDistanceY;

        for (int x = 0; x < GridW; x++)
        {
            for (int y = 0; y < GridH; y++)
            {
                fallDistanceY = (y * gridSpacingY) + Random.Range(0.0f, gridSpacingY) - gridStartY + (GridH * gridSpacingY);

                charmGrid[x, y].SetFalling(fallDistanceY);
            }
        }

        stateMachine.ChangeState(boardStateFalling, this);

    }
    // Clear temp matching grid.
    private void ClearTempPieces()
    {
        for (int x = 0; x < GridW; x++)
        {
            for (int y = 0; y < GridH; y++)
            {
                tempPieces[x, y] = false;
            }
        }
    }

    // Recursively check for possible matches.
    private int CheckForMatch(int x, int y, CharmType type, CharmColor color)
    {
        CharmType thisType;
        CharmColor thisColor;
        int matches = 0;

        if ((x < 0) || (x >= GridW) || (y < 0) || (y >= GridH))
        {
            return 0;
        }

        if (tempPieces[x, y] == true)
        {
            return 0;
        }

        thisType = charmGrid[x, y].CharmType;
        thisColor = charmGrid[x, y].CharmColor;

        if ((type != thisType) && (color != thisColor))
        {
            return 0;
        }


        tempPieces[x, y] = true;

        matches += CheckForMatch(x + 1, y, thisType, thisColor);
        matches += CheckForMatch(x - 1, y, thisType, thisColor);
        matches += CheckForMatch(x, y + 1, thisType, thisColor);
        matches += CheckForMatch(x, y - 1, thisType, thisColor);

        if (matches == 0)
        {
            tempPieces[x, y] = false;
        }

        return matches + 1;
    }

    // Reset hint timer and cancel hint.
    private void ResetHintTimer()
    {
        hintTimer = UnityEngine.Random.Range(5.0f, 10.0f);
        hintSparkle.Stop();
    }

    // Can two Charms be connected either by symbol or color?
    private bool DoesCharmConnect(Charm firstCharm, Charm secondCharm)
    {
        if (firstCharm == null)
        {
            return true;
        }

        if (secondCharm == null)
        {
            return false;
        }

        int x1 = firstCharm.ColX;
        int y1 = firstCharm.RowY;
        int x2 = secondCharm.ColX;
        int y2 = secondCharm.RowY;
        CharmColor color1 = firstCharm.CharmColor;
        CharmType type1 = firstCharm.CharmType;
        CharmColor color2 = secondCharm.CharmColor;
        CharmType type2 = secondCharm.CharmType;
        bool isConnected;

        if ((Mathf.Abs(x1 - x2) + Mathf.Abs(y1 - y2)) <= 1)
        {
            if (selectionGrid[x1, y1].Selected && !selectionGrid[x2, y2].Selected)
            {
                isConnected = ((color1 == color2) || (type1 == type2));

                if (isConnected)
                {
                    return true;
                }
            }
        }

        return false;
    }

    // Display level progress and score, and start level up if progress >= 100%
    private void AddLevelProgress(Vector2 scorePos, Color scoreColor, int numSelected)
    {
        // Add score float
        int scoreToAdd = 10 * numSelected * numSelected;
        int fontSize = Mathf.Clamp(scoreToAdd / 10, 64, 128);

        scorePos /= numSelected;
        stats.Score += scoreToAdd;

        textFloatManager.AddTextFloat(scoreColor, scoreToAdd.ToString(), scorePos, fontSize);

        int maxProgress = level.GetLevelThreshold(stats.Level);

        stats.Progress += MatchLen;
        Debug.Log("Progress:" + stats.Progress + " maxProgress:" + maxProgress + " MatchLen:" + MatchLen);

        if (stats.Progress >= maxProgress)
        {
            stateMachine.ChangeState(boardStateLevelUp, this);

            // Remove all
            DeselectAll();

            for (int x = 0; x < GridW; x++)
            {
                for (int y = 0; y < GridH; y++)
                {
                    Select(x, y, true);
                }
            }

            RemoveLine(true);
        }
    }

    // Is the player going back on the line they have already created?
    private bool IsPrevSelected(int x, int y)
    {
        if (selectionLine.Count < 2)
        {
            return false;
        }

        Selection sel = selectionLine[selectionLine.Count - 2];

        return (sel.ColX == x && sel.RowY == y);
    }

    // Toggle selection in the selection grid, and add/remove from selection line.
    private void Select(int selx, int sely, bool val)
    {
        selectionGrid[selx, sely].Selected = val;

        if (val)
        {
            selectionLine.Add(selectionGrid[selx, sely]);
        }
        else
        {
            selectionLine.Remove(selectionGrid[selx, sely]);
        }
    }

    // Cancel selection.
    private void DeselectAll()
    {
        for (int x = 0; x < GridW; x++)
        {
            for (int y = 0; y < GridH; y++)
            {
                selectionGrid[x, y].Selected = false;
            }
        }

        curCharm = null;
        selectionLine.Clear();
    }

    // Swap the contents of two charms, but leave the positions the same.
    private void SwapCharms(ref Charm charm1, ref Charm charm2)
    {
        Charm temp;
        int charm1x = charm1.ColX;
        int charm1y = charm1.RowY;
        int charm2x = charm2.ColX;
        int charm2y = charm2.RowY;

        temp = charm1;
        charm1 = charm2;
        charm2 = temp;

        charm1.SetPos(charm1x, charm1y);
        charm2.SetPos(charm2x, charm2y);

        charm1.RefreshDisplayPos();
        charm2.RefreshDisplayPos();
    }

    // Translate charm type to sprite.
    /*public Sprite GetCharmSprite(CharmType charm)
    {
        return charmSprites[(int)charm-1];
    }*/
}
